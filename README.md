# README #

The plugin formats any default date format within a Joomla-article into a predefined, language dependend date format.

### How to use it? ###

* Install the plg_format_date.zip with the Joomla Install Manager, Enable the plugin
* You can use the plugin with the following parameters:
{format_date}DATE_FORMAT_LC2|2014-11-14 15:05:41{/format_date}
This will render the default date format into any desired format according the Joomla DATE_FORMAT-rules:

```
#!php

DATE_FORMAT_LC="l, d F Y"
DATE_FORMAT_LC1="l, d F Y"
DATE_FORMAT_LC2="l, d F Y H:i"
DATE_FORMAT_LC3="d F Y"
DATE_FORMAT_LC4="Y-m-d"
DATE_FORMAT_JS1="y-m-d"
```




### Who do I talk to? ###

* Bernhard Sturm (bs@sturmundbraem.ch)
* http://www.sturmundbraem.ch