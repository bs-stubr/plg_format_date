<?php
/**
* @version		$Id: Format Date v1.0.0 2014-11-06 17:50 $
* @package		Joomla 1.6
* @copyright	Copyright (C) 2014 Bernhard Sturm / Sturm und Bräm GmbH. All rights reserved.
* @author		Bernhard Sturm
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/
 
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.plugin.plugin');


class plgContentformat_date extends JPlugin
{
	function plgContentformat_date( &$subject, $params ) 
	{
		parent::__construct( $subject, $params );
	}

	function onContentPrepare($context, &$row, &$params, $limitstart)
	{	
		$output = $row->text;
		$regex = "#{format_date}(.*?){/format_date}#s";
		$found = preg_match_all($regex, $output, $matches);
			
		$count = 0;

		if ( $found )
		{
			foreach ( $matches[0] as $value ) 
			{										
				
				$formatDate = $value;
				$formatDate = str_replace('{format_date}','', $formatDate);
				$formatDate = str_replace('{/format_date}','', $formatDate);

				if (strpos($formatDate,'|') !== false):
					$paramsDate = explode("|",$formatDate); // Parameter
					$formatDate = trim($paramsDate[1]);
					$formatParam = trim($paramsDate[0]);
					
				else:
					$formatParam = "DATE_FORMAT_LC2";

				endif;

				$format = JText::_($formatParam);

				// Generate HTML code for the date
				$replacement[$count] = JHTML::_('date', $formatDate, $format);


				// Increase counter
				$count++;
			}
			for( $i = 0; $i < count($replacement); $i++ )
			{
				$row->text = preg_replace( $regex, $replacement[$i], $row->text,1);
			}
		}
		return true;
	}	
}

?>
